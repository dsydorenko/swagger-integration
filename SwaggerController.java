package controllers;

import play.Play;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class SwaggerController extends Controller {

    public Result swagger() {
        return ok(views.html.swaggerui.render());
    }
    
    public Result readFile() {
    	ObjectNode node = (ObjectNode) Json.parse(Play.application().resourceAsStream("documentation.json"));
    	node.put("basePath", Play.application().configuration().getString("swagger.api.basepath", ""));
    	return ok(node);
    }
}
